/*
 * pizzaDefs2.h
 *
 *  Created on: Dec 28, 2018
 *      Author: irmica
 */

#ifndef PIZZADEFS2_H_
#define PIZZADEFS2_H_

enum btn_event {NONE, INIT_APP, PIZZA_TIMER_START, PIZZA_TIMER_STOP, PAGE_CHANGE, INC_TIMER, DEC_TIMER};
enum led_event {LED_ON, LED_OFF, LED_BLINK};

struct btn_msg
{
	btn_event event_type = NONE;
	short int data = 0;
};

struct led_msg
{
	led_event event_type = LED_OFF;
	short int data = 0;
};

struct btn2led_msg
{
	led_event event_type = LED_OFF;
	short int data = 0;
};

#define NO_OF_PIZZAS 2
#define TIMER_FACTOR 1000
#define MAIN_TIMER_MAX 7200
#define BTN_NO 5
#define LED_NO NO_OF_PIZZAS

#endif /* PIZZADEFS2_H_ */
