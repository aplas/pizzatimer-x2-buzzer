// -alpha_dither yes -dither 2 -non_opaque_image_format ARGB8888 -opaque_image_format RGB565 0xb716c76b
// Generated by imageconverter. Please, do not edit!

#include <touchgfx/Bitmap.hpp>
#include <BitmapDatabase.hpp>

#ifndef NO_USING_NAMESPACE_TOUCHGFX
using namespace touchgfx;
#endif

extern const unsigned char _background[];
extern const unsigned char _blue_progressindicators_bg_large_circle_indicator_bg_line_half[];
extern const unsigned char _blue_progressindicators_fill_large_circle_indicator_fill_line_half[];
extern const unsigned char _pasted_svg_184409x39[];

const touchgfx::Bitmap::BitmapData bitmap_database[] =
{
    { _background, 0, 480, 272, 0, 0, 480, 272, touchgfx::Bitmap::RGB565 },
    { _blue_progressindicators_bg_large_circle_indicator_bg_line_half, 0, 184, 94, 62, 5, 60, 14, touchgfx::Bitmap::ARGB8888 },
    { _blue_progressindicators_fill_large_circle_indicator_fill_line_half, 0, 184, 94, 63, 7, 58, 10, touchgfx::Bitmap::ARGB8888 },
    { _pasted_svg_184409x39, 0, 184, 40, 152, 5, 27, 26, touchgfx::Bitmap::ARGB8888 }
};

namespace BitmapDatabase
{
const touchgfx::Bitmap::BitmapData* getInstance()
{
    return bitmap_database;
}
uint16_t getInstanceSize()
{
    return (uint16_t)(sizeof(bitmap_database) / sizeof(touchgfx::Bitmap::BitmapData));
}
}

