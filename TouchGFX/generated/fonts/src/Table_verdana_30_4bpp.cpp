
#include <touchgfx/Font.hpp>

#ifndef NO_USING_NAMESPACE_TOUCHGFX
using namespace touchgfx;
#endif

FONT_LOCATION_FLASH_PRAGMA
KEEP extern const touchgfx::GlyphNode glyphs_verdana_30_4bpp[] FONT_LOCATION_FLASH_ATTRIBUTE =
{
    {     0,  32,   0,   0,   0,   0,  11, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {     0, 929,  16,  22,  22,   2,  18, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {   176, 935,  19,  22,  22,   1,  21, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {   396, 951,  15,  23,  17,   2,  19, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {   580, 952,  15,  23,  23,   2,  19, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {   764, 953,   4,  17,  17,   2,   8, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {   798, 956,  15,  23,  17,   2,  19, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {   982, 957,  17,  17,  17,   0,  18, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  1135, 959,  16,  17,  17,   1,  18, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  1271, 961,  16,  23,  17,   2,  19, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  1455, 963,  18,  17,  17,   1,  19, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  1608, 965,  15,  17,  17,   2,  19, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  1744, 972,  16,  25,  25,   1,  18, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  1944, 973,  15,  25,  25,   2,  19, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0}
};

