
#include <touchgfx/Font.hpp>

#ifndef NO_USING_NAMESPACE_TOUCHGFX
using namespace touchgfx;
#endif

FONT_LOCATION_FLASH_PRAGMA
KEEP extern const touchgfx::GlyphNode glyphs_verdana_80_4bpp[] FONT_LOCATION_FLASH_ATTRIBUTE =
{
    {     0,  34,  25,  23,  61,   6,  37, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {   299,  39,  10,  23,  61,   6,  21, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {   414,  48,  41,  61,  60,   5,  51, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  1695,  49,  33,  59,  59,  10,  51, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  2698,  50,  40,  60,  60,   6,  51, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  3898,  51,  39,  61,  60,   6,  51, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  5118,  52,  44,  59,  59,   3,  51, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  6416,  53,  39,  60,  59,   7,  51, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  7616,  54,  42,  61,  60,   5,  51, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  8897,  55,  40,  59,  59,   6,  51, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    { 10077,  56,  43,  61,  60,   4,  51, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    { 11419,  57,  42,  61,  60,   4,  51, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    { 12700,  58,  10,  44,  44,  13,  36, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0}
};

