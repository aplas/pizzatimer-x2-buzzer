#include <stdint.h>
#include <touchgfx/Unicode.hpp>

#ifndef NO_USING_NAMESPACE_TOUCHGFX
using namespace touchgfx;
#endif



// Language Gr: No substitution
TEXT_LOCATION_FLASH_PRAGMA
KEEP const touchgfx::Unicode::UnicodeChar T_SingleUseId2_Gr[4] TEXT_LOCATION_FLASH_ATTRIBUTE = { 0x2, 0x3a, 0x2, 0x0 };
TEXT_LOCATION_FLASH_PRAGMA
KEEP const touchgfx::Unicode::UnicodeChar T_SingleUseId3_Gr[2] TEXT_LOCATION_FLASH_ATTRIBUTE = { 0x31, 0x0 };
TEXT_LOCATION_FLASH_PRAGMA
KEEP const touchgfx::Unicode::UnicodeChar T_SingleUseId4_Gr[2] TEXT_LOCATION_FLASH_ATTRIBUTE = { 0x32, 0x0 };
TEXT_LOCATION_FLASH_PRAGMA
KEEP const touchgfx::Unicode::UnicodeChar T_SingleUseId5_Gr[15] TEXT_LOCATION_FLASH_ATTRIBUTE = { 0x3a1, 0x3cd, 0x3b8, 0x3bc, 0x3b9, 0x3c3, 0x3b7, 0x20, 0x3a7, 0x3c1, 0x3cc, 0x3bd, 0x3bf, 0x3c5, 0x0 };
TEXT_LOCATION_FLASH_PRAGMA
KEEP const touchgfx::Unicode::UnicodeChar T_SingleUseId6_Gr[6] TEXT_LOCATION_FLASH_ATTRIBUTE = { 0x2, 0x27, 0x3a, 0x2, 0x22, 0x0 };
TEXT_LOCATION_FLASH_PRAGMA
KEEP const touchgfx::Unicode::UnicodeChar T_SingleUseId9_Gr[3] TEXT_LOCATION_FLASH_ATTRIBUTE = { 0x30, 0x30, 0x0 };

TEXT_LOCATION_FLASH_PRAGMA
KEEP extern const touchgfx::Unicode::UnicodeChar* const textsGr[12] TEXT_LOCATION_FLASH_ATTRIBUTE =
{
    T_SingleUseId2_Gr,
    T_SingleUseId3_Gr,
    T_SingleUseId4_Gr,
    T_SingleUseId5_Gr,
    T_SingleUseId6_Gr,
    T_SingleUseId2_Gr,
    T_SingleUseId9_Gr,
    T_SingleUseId9_Gr,
    T_SingleUseId9_Gr,
    T_SingleUseId9_Gr,
    T_SingleUseId9_Gr,
    T_SingleUseId9_Gr
};

