#include <gui/model/Model.hpp>
#include <gui/model/ModelListener.hpp>

#include "FreeRTOS.h"
#include "queue.h"
#include "stm32f7xx_hal.h"
#include "string.h"
#include "timers.h"

extern xQueueHandle led_msg_q; //From LEDTask
extern xQueueHandle buzzer_msg_q; //From LEDTask

xQueueHandle gui_msg_q;

TimerHandle_t pizzaTimer[3];
unsigned int timerCounter[NO_OF_PIZZAS + 1];
extern UART_HandleTypeDef huart1;

static void prvPizza1TimerCallback(TimerHandle_t xTimer);
static void prvPizza2TimerCallback(TimerHandle_t xTimer);
void setBuzzer(bool state);
void updateProgress1(unsigned int value);

Model::Model() : modelListener(0)
{
	timerCounter[1] = timerCounter[2] = mainTimer = 5;

	gui_msg_q = xQueueGenericCreate(1, sizeof(btn_msg), 0);
	pizzaTimer[1] = xTimerCreate("timer0", pdMS_TO_TICKS(TIMER_FACTOR), pdTRUE, 0, prvPizza1TimerCallback);
	pizzaTimer[2] = xTimerCreate("timer1", pdMS_TO_TICKS(TIMER_FACTOR), pdTRUE, 0, prvPizza2TimerCallback);

}

void Model::tick()
{
	btn_msg btnMsg;

	if (xQueueReceive(gui_msg_q, &btnMsg, 0) == pdTRUE)
	{
		switch(btnMsg.event_type)
		{
		case PIZZA_TIMER_START:
			startPizzaTimer(btnMsg);
			break;
		case PIZZA_TIMER_STOP:
			stopPizzaTimer(btnMsg);
			break;
		case PAGE_CHANGE:
			pageBtnPressed();
			break;
		case INC_TIMER:
			incTimers();
			break;
		case DEC_TIMER:
			decTimers();
			break;
		case NONE: 		break;
		case INIT_APP:	break;

		}
	}

	modelListener->updateTimer1(timerCounter[1] / 60, timerCounter[1] % 60);
	modelListener->updateTimer2(timerCounter[2] / 60, timerCounter[2] % 60);
	modelListener->updatePizzaProgress1((mainTimer - timerCounter[1]) * 100 / mainTimer);
	modelListener->updatePizzaProgress2((mainTimer - timerCounter[2]) * 100 / mainTimer);
}

void Model::incTimers()
{
	if (!xTimerIsTimerActive(pizzaTimer[1]) && !xTimerIsTimerActive(pizzaTimer[2]) && settingsActiveFlag && mainTimer < MAIN_TIMER_MAX)
	{
		mainTimer = mainTimer + 5;
		xTimerChangePeriod(pizzaTimer[1], TIMER_FACTOR, 0);
		xTimerChangePeriod(pizzaTimer[2], TIMER_FACTOR, 0);
		xTimerStop(pizzaTimer[1], 0);
		xTimerStop(pizzaTimer[2], 0);
		modelListener->updateMainTimer(mainTimer / 60, mainTimer % 60);
	}
}

void Model::decTimers()
{
	if (!xTimerIsTimerActive(pizzaTimer[1]) && !xTimerIsTimerActive(pizzaTimer[2]) && settingsActiveFlag && mainTimer > 5)
	{
		mainTimer = mainTimer - 5;
		xTimerChangePeriod(pizzaTimer[1], TIMER_FACTOR, 0);
		xTimerChangePeriod(pizzaTimer[2], TIMER_FACTOR, 0);
		xTimerStop(pizzaTimer[1], 0);
		xTimerStop(pizzaTimer[2], 0);
		modelListener->updateMainTimer(mainTimer / 60, mainTimer % 60);
	}
}

void Model::startPizzaTimer(btn_msg msg)
{
	if (!settingsActiveFlag)
	{
		led_msg ledMsg;
		timerCounter[msg.data] = mainTimer;
		ledMsg.data = msg.data;
		ledMsg.event_type = LED_ON;
		xQueueSend(led_msg_q, &ledMsg, 0);
		xTimerStart(pizzaTimer[msg.data], 0);

		setBuzzer(false);

	}
}
void Model::stopPizzaTimer(btn_msg msg)
{
	if (!settingsActiveFlag)
	{
		led_msg ledMsg;
		ledMsg.data = msg.data;
		ledMsg.event_type = LED_OFF;
		xQueueSend(led_msg_q, &ledMsg, 0);
		xTimerStop(pizzaTimer[msg.data], 0);
	}
}

void Model::pageBtnPressed()
{
	modelListener->togglePage();
}

void Model::setSettingsPageState(bool state)
{
	settingsActiveFlag = state;
}

void Model::getMainTimer(unsigned int& minsValue, unsigned int& secsValue)
{
	minsValue = mainTimer / 60;
	secsValue = mainTimer % 60;
}

void setBuzzer(bool state)
{
	led_msg buzzerMsg;

	if (state)
	{
		buzzerMsg.data = 0;
		buzzerMsg.event_type = LED_ON;
		xQueueSend(buzzer_msg_q, &buzzerMsg, 0);
	}
	else
	{
		buzzerMsg.data = 0;
		buzzerMsg.event_type = LED_OFF;
		xQueueSend(buzzer_msg_q, &buzzerMsg, 0);
	}

}

static void prvPizza1TimerCallback(TimerHandle_t xTimer)
{
	timerCounter[1] -= 1;

	char urtMessage[50];

	sprintf(urtMessage, "timer 1 Counter %d\r\n", timerCounter[1]);
	HAL_UART_Transmit(&huart1, (uint8_t*)urtMessage, strlen(urtMessage), 0xFFFFFFFFU);

	if (timerCounter[1] == 0)
	{
		xTimerStop(pizzaTimer[1], 0);

		led_msg ledMsg;
		ledMsg.data = 1;
		ledMsg.event_type = LED_OFF;
		xQueueSend(led_msg_q, &ledMsg, 0);

		setBuzzer(true);
	}
}

static void prvPizza2TimerCallback(TimerHandle_t xTimer)
{
	timerCounter[2] -= 1;

	char urtMessage[50];

	sprintf(urtMessage, "timer 2 Counter %d\r\n", timerCounter[2]);
	HAL_UART_Transmit(&huart1, (uint8_t*)urtMessage, strlen(urtMessage), 0xFFFFFFFFU);

	if (timerCounter[2] == 0)
	{
		xTimerStop(pizzaTimer[2], 0);

		led_msg ledMsg;
		ledMsg.data = 2;
		ledMsg.event_type = LED_OFF;
		xQueueSend(led_msg_q, &ledMsg, 0);

		setBuzzer(true);
	}
}
