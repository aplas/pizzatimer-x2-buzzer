#include <gui/mainscreen_screen/mainScreenView.hpp>

mainScreenView::mainScreenView()
{

}

void mainScreenView::setupScreen()
{
    mainScreenViewBase::setupScreen();
}

void mainScreenView::tearDownScreen()
{
    mainScreenViewBase::tearDownScreen();
}

void mainScreenView::setTimer1(unsigned int mins, unsigned int secs)
{
    Unicode::snprintf(textTime1Buffer1, TEXTTIME1BUFFER1_SIZE, "%02d", mins);
    Unicode::snprintf(textTime1Buffer2, TEXTTIME1BUFFER1_SIZE, "%02d", secs);
    textTime1.invalidate();
}

void mainScreenView::setTimer2(unsigned int mins, unsigned int secs)
{
    Unicode::snprintf(textTime2Buffer1, TEXTTIME2BUFFER1_SIZE, "%02d", mins);
    Unicode::snprintf(textTime2Buffer2, TEXTTIME2BUFFER2_SIZE, "%02d", secs);
    textTime2.invalidate();
}

void mainScreenView::initProgress1(unsigned int min, unsigned int max)
{
	circleProgress1.setRange(min, max, 1, 1);
}

void mainScreenView::initProgress2(unsigned int min, unsigned int max)
{
	circleProgress2.setRange(min, max, 1, 1);
}

void mainScreenView::setProgress1(unsigned int value)
{
	circleProgress1.setValue(value);
}

void mainScreenView::setProgress2(unsigned int value)
{
	circleProgress2.setValue(value);
}
