#include <gui/mainscreen_screen/mainScreenView.hpp>
#include <gui/mainscreen_screen/mainScreenPresenter.hpp>

mainScreenPresenter::mainScreenPresenter(mainScreenView& v)
    : view(v)
{
}

void mainScreenPresenter::activate()
{
}

void mainScreenPresenter::deactivate()
{

}

void mainScreenPresenter::togglePage()
{
	static_cast<FrontendApplication*>(Application::getInstance())->gotosettingsScreenScreenSlideTransitionEast();
}

void mainScreenPresenter::updateTimer1(unsigned int mins, unsigned int secs)
{
	view.setTimer1(mins, secs);
}

void mainScreenPresenter::updateTimer2(unsigned int mins, unsigned int secs)
{
	view.setTimer2(mins, secs);
}

void mainScreenPresenter::initPizzaProgress1(unsigned int min, unsigned int max)
{
	view.initProgress1(min, max);
}
void mainScreenPresenter::initPizzaProgress2(unsigned int min, unsigned int max)
{
	view.initProgress2(min, max);
}

void mainScreenPresenter::updatePizzaProgress1(unsigned int value)
{
	view.setProgress1(value);
}

void mainScreenPresenter::updatePizzaProgress2(unsigned int value)
{
	view.setProgress2(value);
}
