#include <gui/settingsscreen_screen/settingsScreenView.hpp>

settingsScreenView::settingsScreenView()
{

}

void settingsScreenView::setupScreen()
{
    settingsScreenViewBase::setupScreen();
}

void settingsScreenView::tearDownScreen()
{
    settingsScreenViewBase::tearDownScreen();
}

void settingsScreenView::setMainTimer(unsigned int mins, unsigned int secs)
{
    Unicode::snprintf(textTimeSettingBuffer1, TEXTTIMESETTINGBUFFER1_SIZE, "%02d", mins);
    Unicode::snprintf(textTimeSettingBuffer2, TEXTTIMESETTINGBUFFER2_SIZE, "%02d", secs);
    textTimeSetting.invalidate();
}
