#include <gui/settingsscreen_screen/settingsScreenView.hpp>
#include <gui/settingsscreen_screen/settingsScreenPresenter.hpp>

settingsScreenPresenter::settingsScreenPresenter(settingsScreenView& v)
    : view(v)
{
}

void settingsScreenPresenter::activate()
{
	model->setSettingsPageState(true);
	unsigned int mins, secs;
	model->getMainTimer(mins, secs);
	updateMainTimer(mins, secs);
}

void settingsScreenPresenter::deactivate()
{
	model->setSettingsPageState(false);
}


void settingsScreenPresenter::togglePage()
{
	static_cast<FrontendApplication*>(Application::getInstance())->gotomainScreenScreenSlideTransitionWest();
}

void settingsScreenPresenter::updateMainTimer(unsigned int mins, unsigned int secs)
{
	view.setMainTimer(mins, secs);
}
