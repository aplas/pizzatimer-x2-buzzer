#ifndef MODELLISTENER_HPP
#define MODELLISTENER_HPP

#include <gui/model/Model.hpp>

/**
 * ModelListener is the interface through which the Model can inform the currently
 * active presenter of events. All presenters should derive from this class.
 * It also provides a model pointer for the presenter to interact with the Model.
 *
 * The bind function is called automatically.
 *
 * Add the virtual functions Model should be able to call on the active presenter to this class.
 */
class ModelListener
{
public:
    ModelListener() : model(0) {}

    virtual ~ModelListener() {}

    virtual void togglePage() {}
    virtual void updateTimer1(unsigned int mins, unsigned int secs) {}
    virtual void updateTimer2(unsigned int mins, unsigned int secs) {}
    virtual void updateMainTimer(unsigned int mins, unsigned int secs) {}

    virtual void initPizzaProgress1(unsigned int min, unsigned int max) {}
    virtual void initPizzaProgress2(unsigned int min, unsigned int max) {}

    virtual void updatePizzaProgress1(unsigned int value) {}
	virtual void updatePizzaProgress2(unsigned int value) {}

    /**
     * Sets the model pointer to point to the Model object. Called automatically
     * when switching screen.
     */
    void bind(Model* m)
    {
        model = m;
    }

protected:
    Model* model;
};

#endif /* MODELLISTENER_HPP */
