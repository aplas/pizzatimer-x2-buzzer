#ifndef MAINSCREEN_PRESENTER_HPP
#define MAINSCREEN_PRESENTER_HPP

#include <gui/model/ModelListener.hpp>
#include <mvp/Presenter.hpp>

using namespace touchgfx;

class mainScreenView;

class mainScreenPresenter : public Presenter, public ModelListener
{
public:
    mainScreenPresenter(mainScreenView& v);

    /**
     * The activate function is called automatically when this screen is "switched in"
     * (ie. made active). Initialization logic can be placed here.
     */
    virtual void activate();

    /**
     * The deactivate function is called automatically when this screen is "switched out"
     * (ie. made inactive). Teardown functionality can be placed here.
     */
    virtual void deactivate();

    virtual ~mainScreenPresenter() {};

    virtual void togglePage();

    virtual void updateTimer1(unsigned int mins, unsigned int secs);
    virtual void updateTimer2(unsigned int mins, unsigned int secs);

    virtual void initPizzaProgress1(unsigned int min, unsigned int max);
    virtual void initPizzaProgress2(unsigned int min, unsigned int max);

    virtual void updatePizzaProgress1(unsigned int value);
	virtual void updatePizzaProgress2(unsigned int value);

private:
    mainScreenPresenter();

    mainScreenView& view;
};


#endif // MAINSCREEN_PRESENTER_HPP
