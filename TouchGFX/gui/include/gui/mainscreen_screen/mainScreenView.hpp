#ifndef MAINSCREEN_VIEW_HPP
#define MAINSCREEN_VIEW_HPP

#include <gui_generated/mainscreen_screen/mainScreenViewBase.hpp>
#include <gui/mainscreen_screen/mainScreenPresenter.hpp>

class mainScreenView : public mainScreenViewBase
{
public:
    mainScreenView();
    virtual ~mainScreenView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();

    void setTimer1(unsigned int mins, unsigned int secs);
    void setTimer2(unsigned int mins, unsigned int secs);

    void initProgress1(unsigned int min, unsigned int max);
    void initProgress2(unsigned int min, unsigned int max);

    void setProgress1(unsigned int value);
	void setProgress2(unsigned int value);

protected:
};

#endif // MAINSCREEN_VIEW_HPP
