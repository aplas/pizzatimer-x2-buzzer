#ifndef SPLASHSREEN_VIEW_HPP
#define SPLASHSREEN_VIEW_HPP

#include <gui_generated/splashsreen_screen/splashSreenViewBase.hpp>
#include <gui/splashsreen_screen/splashSreenPresenter.hpp>

class splashSreenView : public splashSreenViewBase
{
public:
    splashSreenView();
    virtual ~splashSreenView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();
protected:
};

#endif // SPLASHSREEN_VIEW_HPP
